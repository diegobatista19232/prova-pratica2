using NUnit.Framework;


namespace ProvaPratica.Tests
{
    public class Tests
    {
        [Test]
        public void CPF()
        {
            
            Assert.AreEqual(true, ProvaPratica.Validador.CPF("22494263001"));
        }

        [Test]
        public void CNPJ()
        {
            
            Assert.AreEqual(true, ProvaPratica.Validador.CNPJ("22173934000142"));
        }
    }

}